// src/Player.js
import React from 'react';
import { Card } from 'react-bootstrap';

const Player = ({ nom, equipe, nationalite, numero, age, image }) => {
  return (
    <Card style={{ width: '20rem', padding:'10px', margin:'20px' }}>
      <Card.Img variant="top" src={image} />
      <Card.Body>
        <Card.Title>{nom}</Card.Title>
        <Card.Text style={{fontWeight:'bold'}}>
          <p>Équipe: {equipe}</p>
          <p>Nationalité: {nationalite}</p>
          <p>Numéro: {numero}</p>
          <p>Âge: {age}</p>
        </Card.Text>
      </Card.Body>
    </Card>
  );
};

Player.defaultProps = {
  nom: 'Inconnu',
  equipe: 'Inconnu',
  nationalite: 'Inconnu',
  numero: 0,
  age: 0,
  image: 'https://via.placeholder.com/150'
};

export default Player;
