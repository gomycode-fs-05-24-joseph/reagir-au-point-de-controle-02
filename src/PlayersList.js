// src/PlayersList.js
import React from 'react';
import Player from './Player';
import joueurs from './joueurs';

const PlayersList = () => {
  return (
    <div>
      {joueurs.map((joueur, index) => (
        <Player key={index} {...joueur} />
      ))}
    </div>
  );
};

export default PlayersList;
