const joueurs = [
    {
      nom: "Lionel Messi",
      equipe: "Paris Saint-Germain",
      nationalite: "Argentine",
      numero: 30,
      age: 34,
      image: "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c8/Lionel_Messi_WC2022.jpg/375px-Lionel_Messi_WC2022.jpg"
    },
    {
      nom: "Cristiano Ronaldo",
      equipe: "Manchester United",
      nationalite: "Portugal",
      numero: 7,
      age: 36,
      image: "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d7/Cristiano_Ronaldo_playing_for_Al_Nassr_FC_against_Persepolis%2C_September_2023_%28cropped%29.jpg/375px-Cristiano_Ronaldo_playing_for_Al_Nassr_FC_against_Persepolis%2C_September_2023_%28cropped%29.jpg"
    },
    {
      nom: "Neymar Jr",
      equipe: "Paris Saint-Germain",
      nationalite: "Brésil",
      numero: 10,
      age: 29,
      image: "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bb/Neymar_Jr._with_Al_Hilal%2C_3_October_2023_-_03_%28cropped%29.jpg/375px-Neymar_Jr._with_Al_Hilal%2C_3_October_2023_-_03_%28cropped%29.jpg"
    },
    {
      nom: "Kylian Mbappé",
      equipe: "Paris Saint-Germain",
      nationalite: "France",
      numero: 7,
      age: 22,
      image: "https://upload.wikimedia.org/wikipedia/commons/b/b3/2022_FIFA_World_Cup_France_4%E2%80%931_Australia_-_%287%29_%28cropped%29.jpg"
    }
  ];
  
  export default joueurs;
  